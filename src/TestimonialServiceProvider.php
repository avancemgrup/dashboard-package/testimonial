<?php

namespace Avannubo\Testimonial;

use Illuminate\Support\ServiceProvider;

class TestimonialServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/Testimonial.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'testimonial');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Avannubo\Testimonial\Controllers\TestimonialController');
    }
}
