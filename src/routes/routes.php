<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {

    Route::get('testimonials',[
        'as' => 'testimonials',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@index',
        'middleware' => ['permission:testimonial.view|testimonial.create|testimonial.edit|testimonial.delete']
    ]);
    Route::get('testimonials/add',[
        'as' => 'testimonials-add',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@create',
        'middleware' => ['permission:testimonial.create']
    ]);
    Route::get('testimonials/edit/{id}',[
        'as' => 'testimonials-edit-form',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@edit',
        'middleware' => ['permission:testimonial.edit']
    ]);
    Route::post('testimonials/add',[
        'as' => 'testimonials-add',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@store',
        'middleware' => ['permission:testimonial.create']
    ]);
    Route::put('testimonials/{id}',[
        'as' => 'testimonials-edit',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@update',
        'middleware' => ['permission:testimonial.edit']
    ]);
    Route::delete('testimonials/{id}',[
        'as' => 'testimonials-delete',
        'uses' => '\Avannubo\Testimonial\Controllers\TestimonialController@delete',
        'middleware' => ['permission:testimonial.delete']
    ]);

});