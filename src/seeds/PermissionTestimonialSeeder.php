<?php
namespace Avannubo\Testimonial\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionTestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'testimonial.view',
            'display_name' => 'testimonial  view',
            'description' => 'Ver testimonios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'testimonial.create',
            'display_name' => 'testimonial create',
            'description' => 'Crear testimonios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'testimonial.edit',
            'display_name' => 'testimonial edit',
            'description' => 'Editar testimonios'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'testimonial.delete',
            'display_name' => 'testimonial delete',
            'description' => 'Eliminar testimonios'
        ]);
        $person->save();
    }
}
