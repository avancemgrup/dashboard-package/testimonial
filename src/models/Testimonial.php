<?php

namespace Avannubo\Testimonial\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model{
    protected $fillable = [
        'id',
        'name',
        'avatar',
        'testimonial',
    ];

}
