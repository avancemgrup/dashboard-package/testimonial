@extends('layouts.administration.master')

@section('site-title')
    Testimonios
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Testimonio</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('testimonials') }}">
                        Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    {!! Form::open(array('route' => ['testimonials-edit', $testimonials->id], 'method'=>'PUT', 'enctype' => 'multipart/form-data')) !!}
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Nombre</label>
                        {!! Form::text('name', $testimonials->name, array('placeholder' => 'Nombre','class' => 'form-control','id' => 'name')) !!}
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                        <label for="avatar">Avatar</label>
                        {!! Form::file('avatar', array('accept' => 'image/*','class' => 'form-control','id' => 'avatar')) !!}
                        <span class="text-danger">{{ $errors->first('avatar') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('testimonial') ? 'has-error' : '' }}">
                        <label for="testimonial">Testimonial</label>
                        {!! Form::textarea('testimonial', $testimonials->testimonial, array('placeholder' => 'Comentario','class' => 'form-control','id' => 'testimonial')) !!}
                        <span class="text-danger">{{ $errors->first('testimonial') }}</span>
                    </div>
                    <button type="submit" class="btn btn-success">
                        Actualizar
                    </button>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function read_URL(e) {
            var input = e.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('uploadImage').src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
            document.getElementById('uploadImage').style.display = 'inline-block';
        }

        document.querySelector("input[name=avatar][type=file]").addEventListener('change',read_URL);
    </script>
@endsection