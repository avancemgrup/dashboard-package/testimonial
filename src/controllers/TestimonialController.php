<?php

namespace Avannubo\Testimonial\Controllers;

use App\Http\Controllers\Controller;
use Avannubo\Testimonial\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Session;


class TestimonialController extends Controller{
    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show the index view
     */
    public function  index(Request $request){
        $search = $request->input('search');
        $testimonials = Testimonial::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('testimonial::index', compact('testimonials'));
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method validates the existence of the information in the database,
     * if it exists it redirects a testimonial else show the form for insert the information
     */
    public function create(){
        return view('testimonial::form');
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it inserts it into the database
     * else Shows error
     *
     */
    public function store(Request $request){
        // Validation
        $rules = [
            'name' => 'required|Max:255',
            'avatar' => 'required|image',
            'testimonial' => 'required|Max:400',
        ];
        $this->validate($request, $rules);

        $avatar = $request->file('avatar');
        $avatarName = time(). '.' .$avatar->getClientOriginalExtension();
        Storage::putFileAs('public/avatar', new File($avatar), $avatarName );

        $testimonials = new Testimonial([
            'name' => $request->input('name'),
            'avatar' => $avatarName,
            'testimonial' => $request->input('testimonial'),

        ]);
        if($testimonials->save()){
            return redirect('administration/testimonials')->with(
                'message', 'Testimonio creado correctamente'
            );
        }else{
            return redirect('administration/testimonials')->with(
                'error', 'Testimonio no creado correctamente,intentelo mas tarde'
            );
        }
    }


    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the information in the database,
     * if it exists it redirects a testimonials else show the form for update the information
     */
    public function edit($id){
        $testimonials = Testimonial::find($id);
        if($testimonials){
            return view('testimonial::updateForm',compact('testimonials'));
        }
        return redirect()->route('administration/testimonials')->with(
            'error','Testimonio no encontrado!'
        );
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it update it into the database
     * else Shows error
     */
    public function update(Request $request, $id){
        // Validation
        $testimonials = Testimonial::find($id);
        if($testimonials){
            $rules = [
                'name' => 'required|Max:255',
                'avatar' => 'image',
                'testimonial' => 'required|Max:400',
            ];
            $this->validate($request, $rules);

            ///Valid if the file comes empty
            if($request->file('avatar') != null){

                if (Storage::disk('public')->exists('/avatar/'.$testimonials->avatar))
                {
                   Storage::disk('public')->delete('/avatar/'.$testimonials->avatar);
                }

                $avatar = $request->file('avatar');
                $avatarName = time(). '.' .$avatar->getClientOriginalExtension();
                Storage::putFileAs('public/avatar', new File($avatar), $avatarName );
                $testimonials->avatar = $avatarName;
            }

            $testimonials->name = $request->input('name');
            $testimonials->testimonial = $request->input('testimonial');
            if($testimonials->update()){
                return redirect()->route('testimonials')->with(
                    'message', 'Testimonio actualizado correctamente'
                );
            }else{
                return redirect()->route('testimonials')->with(
                    'error', 'Testimonio no actualizado correctamente,intentelo mas tarde'
                );
            }

        }
        return redirect()->route('testimonials')->with(
            'error','Testimonio no encontrado'
        );
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it delete it into the database and
     * file local else Shows error
     */
    public function delete (Request $request, $id){
        // Validation
        $testimonials = Testimonial::find($id);
        if($testimonials){
            if( $testimonials->delete()) {
                if (Storage::disk('public')->exists('/avatar/' . $testimonials->avatar)) {
                    Storage::disk('public')->delete('/avatar/' . $testimonials->avatar);
                    Session::flash('message','Testimonio eliminado correctamente');
                }
            }else{
                Session::flash('error','Testimonio no eliminado correctamente, intentelo mas tarde');
            }
        }else{
            Session::flash('error','Testimonio no encontrado');
        }
        return redirect()->route('testimonials');
    }




}
