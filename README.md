## Avannubo package testimonial

Testimonial

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/testimonial": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "url": "https://gitlab.com/avancemgrup/dashboard-package/testimonial.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Testimonial\TestimonialServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Testimonial\Seeds\PermissionTestimonialSeeder`
